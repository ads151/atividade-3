# ATIVIDADE 3

1. Baixe esse repositório

```sh
git clone https://github.com/alexellis/href-counter.git
```
ou 
```sh
wget https://github.com/alexellis/href-counter/archive/master.zip

unzip master.zip
```

2. Acesso o diretório e faça build da imagem sem multi-stage

```sh
docker build -t build -f Dockerfile.build .
```

3. Veja o tamanho da imagem com o comando abaixo

```sh
docker image ls | grep build
```

4. Agora faça o build do multistage

```sh
docker build -t multi -f Dockerfile.multi .
```

5. Veja o tamanho da imagem com o comando abaixo

```sh
docker image ls | grep multi
```

6. Faça a instalação do minikube:

6.1. adicione a chave do repositório

```sh
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
```

6.2. adicione o repositório:

```sh
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
```

6.3. instalar kebectl e minikube

```sh
apt-get update && apt-get install -y kubectl

curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \

  && chmod +x minikube && install minikube /usr/local/bin/
```

6.4. instalar o módulo conntrack

```sh
apt-get install conntrack -y
```

7. Inicie o minikube como root

```sh
minikube start --driver=none
```

8. Espere o processo e digite o seguinte comando

```sh
minikube status
```

9. Digite o comando abaixo para listar os nós do cluster:

```sh
kubectl get node
```

10. Crie o arquivo pod.yaml com o seguinte conteúdo:

```sh
apiVersion: v1

kind: Pod

metadata:

  name: myapp-pod

  labels:

    app: myapp

spec:

  containers:

  - name: myapp-container

    image: busybox

    command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 120']
```

11. Execute o comando abaixo:

```sh
kubectl apply -f pod.yaml
```

12. Verifique se o pod está disponível:

```sh
kubectl get pods
```

13. Verifique o log da aplicação:

```sh
kubectl logs myapp-pod
```

14. Espere 2 minutos, e olhe seu pod novamente

```sh
kubectl get pods
```

15. O que Aconteceu?

16. Verifique o log da aplicação

```sh
kubectl logs myapp-pod
```

17. Delete seu pod

```sh
kubectl delete pods myapp-pod
```
ou
```sh
kubectl delete -f /path/pod.yml
```

18. Atualize o arquivo pod.yaml com o seguinte conteúdo:

```sh
apiVersion: v1

kind: Pod

metadata:

  name: myapp-pod

  labels:

    app: myapp

spec:

  containers:

  - name: myapp-container

    image: busybox

    command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 120']

    restartPolicy: Never
```

19. Execute o comando abaixo:

```sh
kubectl apply -f pod.yaml
```

20. Verifique se o pod está disponível

```sh
kubectl get pods
```

21. Verifique o log da aplicação

```sh
kubectl logs myapp-pod
```

22. Espere 2 minutos, e olhe seu pod novamente:

```sh
kubectl get pods
```

23. O que aconteceu?

24. Verifique o log da aplicação

```sh
kubectl logs myapp-pod
```

POSTAR COMO RESPOSTA:

1- Os arquivos dockerfile sem e com multi stage build

2- Resposta comando 15

3- Resposta comando 23

